from django import template

register = template.Library()


@register.filter(name='test_filter')
def test_filter(val):
    return val + 1
