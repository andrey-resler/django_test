from django.core.management.base import BaseCommand, CommandError
from foobar.models import Company


class Command(BaseCommand):
    help = "Print company name"

    def add_arguments(self, parser):
        parser.add_argument("company_ids", nargs="+", type=int)

    def handle(self, *args, **options):
        for company_id in options["company_ids"]:
            try:
                company = Company.objects.get(pk=company_id)
            except Company.DoesNotExist:
                raise CommandError('Company "%s" does not exist' % company_id)

            self.stdout.write(
                self.style.SUCCESS('Company name is "%s"' % company.name)
            )
