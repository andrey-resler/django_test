from django.test import TestCase
from foobar.models import Company, Position, Employee


class TestModels(TestCase):
    def setUp(self):
        oracle = Company.objects.create(name='Oracle')
        developer = Position.objects.create(name='Developer')
        manager = Position.objects.create(name='Manager')
        self.bob = Employee.objects.create(name='Bob', position=developer, company=oracle)
        self.alice = Employee.objects.create(name='Alice', position=manager, company=oracle)

    def test_is_programmer(self):
        self.assertTrue(self.bob.position.name == 'Developer')
        # self.assertTrue(self.bob.position.name == 'Manager')

    def test_is_manager(self):
        self.assertTrue(self.alice.position.name == 'Manager')
        # self.assertTrue(self.alice.position.name == 'Developer')
