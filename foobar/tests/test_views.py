from django.test import TestCase
from django.urls import reverse

# from foobar.models import Company, Position, Employee
from foobar.views import IndexView
from django.test.client import RequestFactory


class TestModels(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.url = reverse('index')

    def test_index(self):
        request = self.factory.get(self.url)  # generates a request
        view = IndexView()
        view.setup(request)
        assert view.test_method() == 1
