from django.db import models


# Create your models here.


class Company(models.Model):
    name = models.CharField(max_length=255)


class Position(models.Model):
    name = models.CharField(max_length=255)


class Employee(models.Model):
    name = models.CharField(max_length=255)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
