from rest_framework import serializers

from foobar.models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    position = serializers.CharField(source='position.name')
    company = serializers.CharField(source='company.name')
    custom_field = serializers.SerializerMethodField()

    class Meta:
        model = Employee
        fields = ['id', 'name', 'position', 'company', 'custom_field']

    def get_custom_field(self, request):
        return {
            'a': 'foo',
            'b': 'bar',
        }
