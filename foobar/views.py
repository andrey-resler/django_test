from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from django_test.tasks import test_task
from foobar.models import Company, Employee


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self):
        try:
            self.request.session['view_count'] += 1
        except KeyError:
            self.request.session['view_count'] = 0

        company = Company.objects.get(id=1)
        _list = Employee.objects.filter(company=company)
        context = {
            'company': company.name,
            'employee_list': _list,
            'answer': 41,
            'count': self.request.session['view_count']
        }
        return context

    def test_method(self):
        print('Test method executed')
        return 1


def foo(request):
    val = 42
    context = {
        'val': val,
    }
    return render(request, 'foo.html', context)


def celery_test(request):
    test_task.delay()
    return HttpResponse('Task sent to broker...')
