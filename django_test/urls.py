"""django_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from foobar import views
from foobar import rest
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'employees', rest.EmployeeViewSet, basename='employees-v2')

urlpatterns = [
    path('api_v2/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('', views.IndexView.as_view(), name='index'),
    path('foo/', views.foo, name='foo_test'),
    path('api/employees/', rest.EmployeeList.as_view(), name='employees'),
    path('api/employees/<int:pk>/', rest.EmployeeDetail.as_view(), name='employee'),
    path('celery_test', views.celery_test, name='celery_test'),
]
