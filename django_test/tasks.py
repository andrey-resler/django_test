from time import sleep
from celery import shared_task


@shared_task
def test_task():
    for i in range(4):
        print(i)
        sleep(1)
    print("DONE")
