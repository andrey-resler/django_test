import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_test.settings')
django.setup()

from foobar.models import Company, Position, Employee

microsoft = Company.objects.create(name='Microsoft')
oracle = Company.objects.create(name='Oracle')
intel = Company.objects.create(name='Intel')

developer = Position.objects.create(name='Developer')
manager = Position.objects.create(name='Manager')
janitor = Position.objects.create(name='Janitor')

Employee.objects.create(name='Jack', company=microsoft, position=developer)
Employee.objects.create(name='Alice', company=microsoft, position=manager)
Employee.objects.create(name='Bob', company=microsoft, position=janitor)
